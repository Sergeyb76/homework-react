import React, { useState } from "react";
import { UserService } from "../../services/UserService";
import "./Authorization.css";

interface AuthorizationProps {}

const Authorization: React.FC<AuthorizationProps> = (props) => {
  const [userName, setUserName] = useState("");
  const [userPassword, setUserPassword] = useState("");

  const submit = async (event: React.FormEvent) => {
    event.preventDefault();

    console.log(userName, userPassword);

    //Some validation
    if (userName === "" || userPassword === "") {
      alert("You should fill Login and Password");
      return;
    }

    // For example use jsonplaceholder
    // Post somthing
    const userService = new UserService();
    userService.authorize(userName, userPassword);
  };

  return (
    <form onSubmit={submit}>
      <div className="a-container">
        <div className="input-container">
          <div className="input-label">Login:</div>
          <input
            type="text"
            className="base-input"
            placeholder="Login"
            onChange={(e) => setUserName(e.target.value)}
          />
        </div>
        <div className="input-container">
          <div className="input-label">Password:</div>
          <input
            type="password"
            className="base-input"
            placeholder="Password"
            onChange={(e) => setUserPassword(e.target.value)}
          />
        </div>
        <button type="submit" className="btn-submit">
          Send
        </button>
      </div>
    </form>
  );
};

export default Authorization;
