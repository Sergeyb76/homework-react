import React from 'react';
import Authorization from './components/Authorization/Authorization';

function App() {
  return (
    <Authorization />
  );
}

export default App;
