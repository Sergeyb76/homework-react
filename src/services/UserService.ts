import axios from "axios";

export class UserService {
  public _apiBase: string;
  constructor() {
    this._apiBase = "https://jsonplaceholder.typicode.com/posts";
  }

  public async authorize(userName: string, userPassword: string) {
    const postData = JSON.stringify({
      title: "User name and password",
      body: `User name: ${userName}, Password: ${userPassword}`,
      userId: 1,
    });

    await axios
      .post(this._apiBase, postData)
      .then((response) => {
        console.log(response.request.response);
      })
      .catch((e) => {
        throw new Error(e.message);
      });
  }
}
